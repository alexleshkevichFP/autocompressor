#!/usr/bin/env node
const {promisify} = require('util');
const {resolve} = require('path');
const fs = require('fs');
const zlib = require('zlib');
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

const [, , ...args] = process.argv;
const dirPath = args[0];
let fileList = []

const getFiles = async (dir) => {
    const subdirs = await readdir(dir);
    const files = await Promise.all(subdirs.map(async (subdir) => {
        const res = resolve(dir, subdir);
        return (await stat(res)).isDirectory() ? getFiles(res) : res;
    }));

    return files.reduce((a, f) => {
        return f.indexOf('.gz') === -1 ? a.concat(f) : a;
    }, []);
}


const getNext = (cb) => {
    if (fileList.length) {
        const file = fileList.shift();

        stat(file + '.gz')
            .then(stats => {
                const zipFileModTime = stats.mtimeMs;

                stat(file)
                    .then(fStats => {
                        const fileModTime = fStats.mtimeMs;

                        if (fileModTime > zipFileModTime) {

                            compressFile(file)
                                .then(result => {
                                    getNext(cb)
                                });
                        } else {
                            getNext(cb)
                        }
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })
            .catch(err => {
                console.log(err)
                compressFile(file)
                    .then(result => {
                        getNext(cb)
                    })
            });
    } else {
        cb();
    }
}

const compressFile = (filename) => {
    return new Promise((resolve, reject) => {
        console.log(`FILE ${filename} ZIP START`);
        const compress = zlib.createGzip();
        const input = fs.createReadStream(filename);
        const output = fs.createWriteStream(filename.indexOf('.gz') > -1 ? filename : filename + '.gz');

        input.pipe(compress).pipe(output).on('close', () => {
            console.log(`FILE ${filename} ZIP END`);
            console.log('Current date: ', new Date());
            resolve('done');
        });
    });
}


getFiles(dirPath)
    .then((files) => {
        fileList = files;

        getNext(() => {
            console.log('compress end')
        })
    })
// console.log(`Path ${dirPath}`);